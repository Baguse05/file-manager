import Vue from "vue";
import App from "./App.vue";

import { DocumentEditorPlugin } from "@syncfusion/ej2-vue-documenteditor";

Vue.use(DocumentEditorPlugin);

new Vue({
  el: "#app",
  render: (h) => h(App),
});
